#!/bin/bash

GCK_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

_gck_list_make_targets() {
    cd ${GCK_PATH}
    make help | grep "^- " | sed "s/- //" | xargs
}

_gck_run_make() {
    current_path=$(pwd)

    cd ${GCK_PATH}
    make ${1}
    result=$?

    cd ${current_path}
    return ${result}
}

gck() {
    case "$1" in
        cd)
            path=${GCK_PATH}
            if [ -n ${2} ]; then
                path="${path}/${2}"
            fi
            cd $path
            ;;
        refresh)
            source ${BASH_SOURCE[0]}
            ;;
        *)
            if [[ "$(_gck_list_make_targets)" == *"${1}"* ]]; then
                _gck_run_make ${1}
                return $?
            fi

            echo "Unknown command"
            return 1
            ;;
    esac
}

_gck_comp() {
    local cur prev opts

    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts="cd refresh $(_gck_list_make_targets)"

    case "${prev}" in
        cd)
            local -a tokens
            local IFS=$'\n' x tmp quoted

            compopt -o nospace

            _quote_readline_by_ref "$cur" quoted
            x=$(compgen -d -- "${GCK_PATH}/${quoted}") &&
            while read -r tmp; do
                tokens+=( "$(echo ${tmp} | sed "s|${GCK_PATH}/||")" )
            done <<< "${x}"

            if [[ ${#tokens[@]} -ne 0 ]]; then
                compopt -o filenames 2>/dev/null
                COMPREPLY+=( "${tokens[@]}" )
            fi

            return 0
            ;;
        *)
            ;;
    esac

    COMPREPLY=($(compgen -W "${opts}" -- ${cur}))
    return 0
}

complete -F _gck_comp gck
